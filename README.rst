============
flufl.bounce
============

Email bounce detectors.

The `flufl.bounce` library provides a set of heuristics and an API for
detecting the original bouncing email addresses from a bounce message.  Many
formats found in the wild are supported, as are VERP_ and RFC 3464 (DSN_).


Authors
=======

`flufl.bounce` is Copyright (C) 2004-2017 Barry Warsaw <barry@python.org>

Barry Warsaw <barry@python.org>
Mark Sapiro <mark@msapiro.net>

Licensed under the terms of the Apache Software License, version 2.  See
LICENSE for details.


Project details
===============

 * Project home: https://gitlab.com/warsaw/flufl.bounce
 * Report bugs at: https://gitlab.com/warsaw/flufl.bounce/issues
 * Code: https://gitlab.com/warsaw/flufl.bounce.git
 * Documentation: http://fluflbounce.readthedocs.io/


.. _VERP: http://en.wikipedia.org/wiki/Variable_envelope_return_path
.. _DSN: http://www.faqs.org/rfcs/rfc3464.html
